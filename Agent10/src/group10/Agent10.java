package group10;

import java.util.List;

import negotiator.AgentID;
import negotiator.Bid;

import negotiator.actions.Accept;
import negotiator.actions.Action;
import negotiator.actions.Offer;

import negotiator.parties.AbstractNegotiationParty;
import negotiator.parties.NegotiationInfo;

import negotiator.issue.Issue;
import negotiator.issue.IssueDiscrete;
import negotiator.issue.Value;
import negotiator.issue.ValueDiscrete;

import negotiator.utility.AbstractUtilitySpace;
import negotiator.utility.AdditiveUtilitySpace;
import negotiator.utility.EvaluatorDiscrete;

/**
 * ExampleAgent returns the bid that maximizes its own utility for half of the negotiation session.
 * In the second half, it offers a random bid. It only accepts the bid on the table in this phase,
 * if the utility of the bid is higher than Example Agent's last bid.
 */
public class Agent10 extends AbstractNegotiationParty {
    private final String description = "Group 10 Agent";

    private Bid lastReceivedOffer;	//offer on the table
    private Bid myLastOffer;
    private Bid myMaxUtilityBid;
    private Bid agent1_Bid, agent2_Bid;

    private boolean flag = true;

    private int numberOfIssue;	//number of issues
    private int numberOfCurrentEvaluation;	//number of current evaluation
    private int sizeOfCurrentEvaluation;	//number of current evaluation

    @Override
    public void init(NegotiationInfo info) {
    		super.init(info);
    	}

    /**
     * When this function is called, it is expected that the Party chooses one of the actions from the possible
     * action list and returns an instance of the chosen action.
     *
     * @param list
     * @return
     */
    @Override
    public Action chooseAction(List<Class<? extends Action>> list) {
        //According to Stacked Alternating Offers Protocol list includes
        //Accept, Offer and EndNegotiation actions only.
        double time = getTimeLine().getTime();	// Gets the time, running from t = 0 (start) to t = 1 (deadline).
        //The time is normalized, so agents need not be
        //concerned with the actual internal clock.
        //System.out.println(time);
        //Initialization and displaying some information
        if (time < 0.01) {
            AbstractUtilitySpace utilitySpace = this.getUtilitySpace();
            AdditiveUtilitySpace additiveUtilitySpace = (AdditiveUtilitySpace) utilitySpace;
            //System.out.println("-------------Space--------------------------");
            //System.out.println("" + additiveUtilitySpace);

            //System.out.println("---------------Max Bid-------------------------------");
            myMaxUtilityBid = this.getMaxUtilityBid();
            //System.out.println("" + myMaxUtilityBid + " MaxUtility is: " + this.utilitySpace.getUtility(myMaxUtilityBid));

            //System.out.println("-----------My issues and weights------------------------");
            List<Issue> issues = additiveUtilitySpace.getDomain().getIssues();	//issues contents in strings
            //System.out.println("" + issues);

            numberOfIssue = issues.size();
            //System.out.println("Number of issues:" + numberOfIssue);

            double myweights[] = new double[numberOfIssue];

            for (Issue i : issues) {
            		//shows the weights
                int issueNumber = i.getNumber();
                myweights[issueNumber - 1] = additiveUtilitySpace.getWeight(i.getNumber());
                System.out.println(additiveUtilitySpace.getWeight(i.getNumber()));
            }
        }
        //------------------------------------------------------------------------------------------------------------
        if (time < 0.03) {
            System.out.println("---Max---");
            return new Offer(this.getPartyId(), this.getMaxUtilityBid());
        }
        else {
            if (lastReceivedOffer != null && myLastOffer != null && this.utilitySpace.getUtility(lastReceivedOffer) >= 0.8 || time >0.985) {
                System.out.println("---Accept---");
                return new Accept(this.getPartyId(), lastReceivedOffer);
            }
            else {
                if(time > 0.03 && time < 0.5) {
                    //Offering a random bid
                    myLastOffer = generateRandomBid();

                    //-----Initialization------
                    AbstractUtilitySpace utilitySpace = this.getUtilitySpace();
                    AdditiveUtilitySpace additiveUtilitySpace = (AdditiveUtilitySpace) utilitySpace;
                    List<Issue> issues = additiveUtilitySpace.getDomain().getIssues();	//issues contents in strings
                    numberOfIssue = issues.size();
                    double MaxEvaluation[] = new double[numberOfIssue];

                    //generate the MaxEvaluation array
                    for (Issue issue : issues) {
                        int issueNumber = issue.getNumber();
                        IssueDiscrete issueDiscrete = (IssueDiscrete) issue;	//discrete
                        EvaluatorDiscrete evaluatorDiscrete = (EvaluatorDiscrete) additiveUtilitySpace.getEvaluator(issueNumber);

                        //calculate the size of evaluation
                        sizeOfCurrentEvaluation = issueDiscrete.getValues().size();

                        //define and save the array and find the max
                        double CurrentEvaluation[] = new double[sizeOfCurrentEvaluation];
                        numberOfCurrentEvaluation = 0;
                        double max = 0.0;
                        for (ValueDiscrete valueDiscrete : issueDiscrete.getValues()) {
                            CurrentEvaluation[numberOfCurrentEvaluation] = evaluatorDiscrete.getValue(valueDiscrete);
                            if (CurrentEvaluation[numberOfCurrentEvaluation] > max) {
                                max = CurrentEvaluation[numberOfCurrentEvaluation];
                                numberOfCurrentEvaluation++;
                            }
                        }
                        int issueNumber2 = issue.getNumber();
                        MaxEvaluation[issueNumber2 - 1] = max;	//this is what we want
                    }

                    //check for greater than 0.75 or not
                    for (Issue issue : issues) {
                        int issueNumber = issue.getNumber();
                        double currentEvaluation = 0.0;

                        Issue currentIssue = lastReceivedOffer.getIssues().get(issueNumber - 1);	//current issue
                        IssueDiscrete issueDiscrete = (IssueDiscrete) currentIssue;	//discrete
                        List<ValueDiscrete> allValues = issueDiscrete.getValues();	//list of all variables in a issue

                        Value currentvalueDiscrete = lastReceivedOffer.getValue(issueNumber);	//discrete
                        IssueDiscrete issueDiscrete2 = (IssueDiscrete) issue;
                        EvaluatorDiscrete evaluatorDiscrete = (EvaluatorDiscrete) additiveUtilitySpace.getEvaluator(issueNumber);
                        //System.out.print("Issue:" + currentvalueDiscrete.toString());

                        numberOfCurrentEvaluation = 0;
                        for (ValueDiscrete valueDiscrete : issueDiscrete2.getValues()) {
                            numberOfCurrentEvaluation++;
                            String a = currentvalueDiscrete.toString();
                            String b = valueDiscrete.getValue();
                            if (a.equals(b)) {
                                //match
                                currentEvaluation = evaluatorDiscrete.getValue(valueDiscrete);
                                //System.out.println("  | eva:" + currentEvaluation);

                                if (currentEvaluation / MaxEvaluation[issueNumber - 1] >= 0.75)
                                {
                                    ValueDiscrete valueDiscrete2 = allValues.get(numberOfCurrentEvaluation - 1);	//Change the food issue
                                    myLastOffer = myLastOffer.putValue(issueNumber, valueDiscrete2);
                                }
                            }
                        }
                    }
                }
                else {
                    //Offering a random bid
                    myLastOffer = generateRandomBid();

                    //-----Initialization------
                    AbstractUtilitySpace utilitySpace = this.getUtilitySpace();
                    AdditiveUtilitySpace additiveUtilitySpace = (AdditiveUtilitySpace) utilitySpace;
                    List<Issue> issues = additiveUtilitySpace.getDomain().getIssues(); //issues contents in strings
                    numberOfIssue = issues.size();
                    double MaxEvaluation[] = new double[numberOfIssue];

                    //generate the MaxEvaluation array
                    for (Issue issue : issues) {
                        int issueNumber = issue.getNumber();
                        IssueDiscrete issueDiscrete = (IssueDiscrete) issue; //discrete
                        EvaluatorDiscrete evaluatorDiscrete = (EvaluatorDiscrete) additiveUtilitySpace.getEvaluator(issueNumber);

                        //calculate the size of evaluation
                        sizeOfCurrentEvaluation = issueDiscrete.getValues().size();

                        //define and save the array and find the max
                        double CurrentEvaluation[] = new double[sizeOfCurrentEvaluation];
                        numberOfCurrentEvaluation = 0;
                        double max = 0.0;
                        for (ValueDiscrete valueDiscrete : issueDiscrete.getValues()) {
                            CurrentEvaluation[numberOfCurrentEvaluation] = evaluatorDiscrete.getValue(valueDiscrete);
                            if (CurrentEvaluation[numberOfCurrentEvaluation] > max) {
                                max = CurrentEvaluation[numberOfCurrentEvaluation];
                                numberOfCurrentEvaluation++;
                            }
                        }
                        int issueNumber2 = issue.getNumber();
                        MaxEvaluation[issueNumber2 - 1] = max;	//this is what we want
                    }

                    //check for greater than 0.65 or not
                    for (Issue issue : issues) {
                        int issueNumber = issue.getNumber();
                        double currentEvaluation = 0.0;

                        Issue currentIssue = lastReceivedOffer.getIssues().get(issueNumber - 1);	//current issue
                        IssueDiscrete issueDiscrete = (IssueDiscrete) currentIssue;	//discrete
                        List<ValueDiscrete> allValues = issueDiscrete.getValues();	//list of all variables in a issue

                        Value currentvalueDiscrete = lastReceivedOffer.getValue(issueNumber);	//discrete
                        IssueDiscrete issueDiscrete2 = (IssueDiscrete) issue;
                        EvaluatorDiscrete evaluatorDiscrete = (EvaluatorDiscrete) additiveUtilitySpace.getEvaluator(issueNumber);
                        //System.out.print("Issue:" + currentvalueDiscrete.toString());

                        numberOfCurrentEvaluation = 0;
                        for (ValueDiscrete valueDiscrete : issueDiscrete2.getValues()) {
                            numberOfCurrentEvaluation++;
                            String a = currentvalueDiscrete.toString();
                            String b = valueDiscrete.getValue();
                            if (a.equals(b)) {
                                //match
                                currentEvaluation = evaluatorDiscrete.getValue(valueDiscrete);
                                //System.out.println("  | eva:" + currentEvaluation);

                                if (currentEvaluation / MaxEvaluation[issueNumber - 1] >= 0.65)
                                {
                                    ValueDiscrete valueDiscrete2 = allValues.get(numberOfCurrentEvaluation - 1);	//Change the food issue
                                    myLastOffer = myLastOffer.putValue(issueNumber, valueDiscrete2);
                                }
                            }
                        }
                    }
                }
                return new Offer(this.getPartyId(), myLastOffer);
            }
        }
    }

    /**
     * This method is called to inform the party that another NegotiationParty chose an Action.
     * @param sender
     * @param act
     */
    @Override
    public void receiveMessage(AgentID sender, Action act) {
        super.receiveMessage(sender, act);

        if (act instanceof Offer) {
        		// sender is making an offer
            Offer offer = (Offer) act;
            // storing last received offer
            lastReceivedOffer = offer.getBid();

            if (flag) {
                agent1_Bid = lastReceivedOffer;
                flag = false;
            }
            else {
                agent2_Bid = lastReceivedOffer;
                flag = true;
            }
        }
    }

    public Bid generateRandomBidWithUtility(double utilityThreshold) {
        Bid randomBid;
        double utility;
        do {
            randomBid = generateRandomBid();
            try {
                utility = utilitySpace.getUtility(randomBid);
            } catch(Exception e) {
                utility = 0.0;
            }
        }
        while(utility < utilityThreshold);
        return randomBid;
    }

    private Bid getMinUtilityBid() {
        try {
            return this.utilitySpace.getMinUtilityBid();
        } catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private Bid getMaxUtilityBid() {
        try {
            return this.utilitySpace.getMaxUtilityBid();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getDescription() {
        // TODO Auto-generated method stub
        return description;
    }
}
